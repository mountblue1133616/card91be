import mongoose from "mongoose";

const cashbackConfig = [
  {
    minAmount: 0,
    maxAmount: 500,
    cashbackFn: (amount, type) => (type === "Ecom" ? amount * 0.02 : 0),
  },
  {
    minAmount: 500,
    maxAmount: 2000,
    cashbackFn: () => 200,
  },
  {
    minAmount: 2000,
    maxAmount: 3000,
    cashbackFn: (amount, type) =>
      type === "Ecom" || type === "Pos" ? Math.max(250, amount * 0.1) : 0,
  },
  {
    minAmount: 3000,
    maxAmount: Infinity,
    cashbackFn: (amount, type) => (type === "Pos" ? amount * 0.15 : 0),
  },
];

const transactionSchema = new mongoose.Schema({
  txnId: String,
  amount: Number,
  type: {
    type: String,
    enum: ["Ecom", "Pos"],
    required: true,
  },
  cashback: {
    type: Number,
    default: 0,
  },
});

transactionSchema.pre("save", function (next) {
  const { amount, type } = this;

  const config = cashbackConfig.find(
    (entry) => amount >= entry.minAmount && amount <= entry.maxAmount
  );

  if (config) {
    this.cashback = config.cashbackFn(amount, type);
  } else {
    this.cashback = 0;
  }

  next();
});

const Transaction = mongoose.model("Transaction", transactionSchema);
export default Transaction;
