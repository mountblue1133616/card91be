import express from "express";

import {
  createTransaction,
  getAllTransactions,
} from "../controllers/transactionController.js";
const route = express.Router();

route.post("/", createTransaction);
route.get("/", getAllTransactions);

export default route;
