import Transaction from "../models/transactions.js";

export const createTransaction = async (req, res) => {
  try {
    const { txnId, amount, type } = req.body;

    const newTransaction = new Transaction({
      txnId,
      amount,
      type,
    });

    await newTransaction.save();

    res.status(201).json({ cashback: newTransaction.cashback });
  } catch (err) {
    console.error(err);
    res.status(500).send("An error occurred while saving the transaction.");
  }
};

export const getAllTransactions = async (req, res) => {
  try {
    const transactions = await Transaction.find({});
    res.json(transactions);
  } catch (error) {
    console.error(error);
    res.status(500).send("An error occurred while retrieving transactions.");
  }
};
